package org.fpwei.bo.dto;

/**
 * Created by fpwei on 2017/1/2.
 */
public class JobDto {
    private String jobName;
    private String jobGroup;
    private String description;
    private boolean isDurable;
    private boolean isNonconcurrent;
    private boolean isUpdateData;
    private boolean requestsRecovery;

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getJobGroup() {
        return jobGroup;
    }

    public void setJobGroup(String jobGroup) {
        this.jobGroup = jobGroup;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isDurable() {
        return isDurable;
    }

    public void setDurable(boolean durable) {
        isDurable = durable;
    }

    public boolean isNonconcurrent() {
        return isNonconcurrent;
    }

    public void setNonconcurrent(boolean nonconcurrent) {
        isNonconcurrent = nonconcurrent;
    }

    public boolean isUpdateData() {
        return isUpdateData;
    }

    public void setUpdateData(boolean updateData) {
        isUpdateData = updateData;
    }

    public boolean isRequestsRecovery() {
        return requestsRecovery;
    }

    public void setRequestsRecovery(boolean requestsRecovery) {
        this.requestsRecovery = requestsRecovery;
    }
}
