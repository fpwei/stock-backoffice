package org.fpwei.bo.dto;

import org.fpwei.bo.entity.MenuItem;

import java.util.List;

/**
 * Created by fpwei on 2017/1/2.
 */
public class MenuItemDto {
    private String displayName;
    private String icon;
    private String file;
    private List<MenuItemDto> subMenuItems;

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public List<MenuItemDto> getSubMenuItems() {
        return subMenuItems;
    }

    public void setSubMenuItems(List<MenuItemDto> subMenuItems) {
        this.subMenuItems = subMenuItems;
    }
}
