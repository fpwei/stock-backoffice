package org.fpwei.bo.entity;

import java.util.List;

/**
 * Created by fpwei on 2017/1/2.
 */
public class MenuItem {
    private int seqno;
    private String displayName;
    private String icon;
    private String file;
    private int parentSeqno;
    private int sort;

    public int getSeqno() {
        return seqno;
    }

    public void setSeqno(int seqno) {
        this.seqno = seqno;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public int getParentSeqno() {
        return parentSeqno;
    }

    public void setParentSeqno(int parentSeqno) {
        this.parentSeqno = parentSeqno;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }
}
