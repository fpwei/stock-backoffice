package org.fpwei.bo.config;

import org.mybatis.spring.mapper.MapperScannerConfigurer;
import org.springframework.context.annotation.*;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.stereotype.Repository;

/**
 * Created by fpwei on 2017/1/2.
 */
@Configuration
@Import({DaoConfiguration.class})
@ImportResource({"classpath:/spring/cxf-beans.xml"})
@PropertySources({@PropertySource("classpath:/config.properties")})
@ComponentScan("org.fpwei.bo")
public class AppConfiguration {
    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer(){
        return new PropertySourcesPlaceholderConfigurer();
    }

    @Bean
    public MapperScannerConfigurer mapperScannerConfigurer() {
        MapperScannerConfigurer mapperScannerConfigurer = new MapperScannerConfigurer();
        mapperScannerConfigurer.setBasePackage("org.fpwei.bo.dao");
        mapperScannerConfigurer.setAnnotationClass(Repository.class);
        mapperScannerConfigurer.setSqlSessionTemplateBeanName("sqlSessionTemplate");

        return mapperScannerConfigurer;
    }
}
