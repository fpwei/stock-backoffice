package org.fpwei.bo.service;

import org.fpwei.bo.dto.MenuItemDto;

import java.util.List;

/**
 * Created by fpwei on 2017/1/2.
 */
public interface MenuItemService {
    List<MenuItemDto> getNavigation();
}
