package org.fpwei.bo.service.impl;

import org.fpwei.bo.dao.NavigationDao;
import org.fpwei.bo.dto.MenuItemDto;
import org.fpwei.bo.entity.MenuItem;
import org.fpwei.bo.service.MenuItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fpwei on 2017/1/2.
 */
@Service("menuItemService")
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)
public class MenuItemServiceImpl implements MenuItemService {
    @Autowired
    private NavigationDao navigationDao;

    @Override
    public List<MenuItemDto> getNavigation() {
        List<MenuItemDto> list = new ArrayList<>();
        List<MenuItem> menuItemList = navigationDao.getMenuItemWithoutParent();

        for (MenuItem menuItem : menuItemList) {
            MenuItemDto dto = convertMenuItemToDto(menuItem);
            list.add(dto);
        }

        return list;
    }

    private MenuItemDto convertMenuItemToDto(MenuItem menuItem) {
        MenuItemDto dto = new MenuItemDto();
        dto.setDisplayName(menuItem.getDisplayName());
        dto.setFile(menuItem.getFile() == null ? null : "WEB-INF/zul/" + menuItem.getFile());
        dto.setIcon(menuItem.getIcon());

        List<MenuItem> subMenuItemList = navigationDao.getMenuItemByParentSeqno(menuItem.getSeqno());
        List<MenuItemDto> subMenuItems = new ArrayList<>();
        for (MenuItem subMenuItem : subMenuItemList) {
            subMenuItems.add(convertMenuItemToDto(subMenuItem));
        }
        dto.setSubMenuItems(subMenuItems);

        return dto;
    }
}
