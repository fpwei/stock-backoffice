package org.fpwei.bo.service.impl;

import org.apache.commons.lang3.StringUtils;
import org.fpwei.bo.dao.QuartzDao;
import org.fpwei.bo.dto.CronTriggerDto;
import org.fpwei.bo.dto.JobDto;
import org.fpwei.bo.entity.Job;
import org.fpwei.bo.entity.QuartzCronTrigger;
import org.fpwei.bo.entity.QuartzTrigger;
import org.fpwei.bo.service.ScheduleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by fpwei on 2017/1/2.
 */
@Service("scheduleService")
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)
public class ScheduleServiceImpl implements ScheduleService {
    @Autowired
    private QuartzDao quartzDao;

    @Override
    public List<JobDto> getAllJobs() {
        List<JobDto> list = new ArrayList<>();
        List<Job> jobList = quartzDao.getAllJobs();

        for (Job job : jobList) {
            JobDto dto = convertJobToDto(job);
            list.add(dto);
        }

        return list;
    }

    @Override
    public List<JobDto> getJobsByName(String jobName) {
        List<JobDto> list = new ArrayList<>();
        List<Job> jobList;
        if(jobName == null){
            jobList = quartzDao.getJobsByName(StringUtils.EMPTY);
        }else{
            jobList = quartzDao.getJobsByName(jobName);
        }

        for (Job job : jobList) {
            JobDto dto = convertJobToDto(job);
            list.add(dto);
        }

        return list;
    }

    @Override
    public List<CronTriggerDto> getAllCronTriggers() {
        List<CronTriggerDto> list = new ArrayList<>();
        List<QuartzCronTrigger> cronTriggerList = quartzDao.getAllCronTriggers();

        for (QuartzCronTrigger cronTrigger : cronTriggerList) {
            QuartzTrigger trigger = quartzDao.getTriggerByTriggerKey(cronTrigger.getTriggerName(), cronTrigger.getTriggerGroup());
            CronTriggerDto dto = convertTriggerToDto(cronTrigger, trigger);
            list.add(dto);
        }

        return list;
    }

    @Override
    public List<CronTriggerDto> getCronTriggersByJobName(String jobName) {
        List<CronTriggerDto> list = new ArrayList<>();
        List<QuartzTrigger> triggerList;
        if(jobName == null){
            triggerList = quartzDao.getTriggerByJobName(StringUtils.EMPTY);
        }else{
            triggerList = quartzDao.getTriggerByJobName(jobName);
        }

        for (QuartzTrigger trigger : triggerList) {
            QuartzCronTrigger cronTrigger = quartzDao.getCronTriggerByTriggerKey(trigger.getTriggerName(), trigger.getTriggerGroup());
            CronTriggerDto dto = convertTriggerToDto(cronTrigger, trigger);
            list.add(dto);
        }

        return list;
    }


    private JobDto convertJobToDto(Job job) {
        JobDto dto = new JobDto();

        dto.setDescription(job.getDescription());
        dto.setDurable(job.isDurable());
        dto.setJobGroup(job.getJobGroup());
        dto.setJobName(job.getJobName());
        dto.setNonconcurrent(job.isNonconcurrent());
        dto.setRequestsRecovery(job.isRequestsRecovery());
        dto.setUpdateData(job.isUpdateData());

        return dto;
    }

    private CronTriggerDto convertTriggerToDto(QuartzCronTrigger cronTrigger, QuartzTrigger trigger) {
        CronTriggerDto dto = new CronTriggerDto();

        dto.setCronExpression(cronTrigger.getCronExpression());
        dto.setTriggerName(trigger.getTriggerName());
        dto.setTriggerGroup(trigger.getTriggerGroup());
        dto.setJobName(trigger.getJobName());
        dto.setJobGroup(trigger.getJobGroup());
        dto.setDescription(trigger.getDescription());
        dto.setPriority(trigger.getPriority());
        dto.setTriggerState(trigger.getTriggerState());
        dto.setMisfireInstr(trigger.isMisfireInstr());
        dto.setPrevFireTime(trigger.getPrevFireTime()==0 ? null :new Date(trigger.getPrevFireTime()));
        dto.setNextFireTime(trigger.getNextFireTime()==0 ? null :new Date(trigger.getNextFireTime()));
        dto.setStartTime(trigger.getStartTime()==0 ? null :new Date(trigger.getStartTime()));
        dto.setEndTime(trigger.getEndTime()==0 ? null :new Date(trigger.getEndTime()));
        dto.setTimeZoneId(cronTrigger.getTimeZoneId());

        return dto;
    }

}
