package org.fpwei.bo.service;

import org.fpwei.bo.dto.CronTriggerDto;
import org.fpwei.bo.dto.JobDto;

import java.util.List;

/**
 * Created by fpwei on 2017/1/2.
 */
public interface ScheduleService {
    List<JobDto> getAllJobs();

    List<JobDto> getJobsByName(String jobName);

    List<CronTriggerDto> getAllCronTriggers();

    List<CronTriggerDto> getCronTriggersByJobName(String jobName);
}
