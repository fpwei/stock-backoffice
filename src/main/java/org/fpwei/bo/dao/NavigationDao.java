package org.fpwei.bo.dao;

import org.fpwei.bo.entity.MenuItem;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by fpwei on 2017/1/2.
 */
@Repository
public interface NavigationDao {
    List<MenuItem> getMenuItemWithoutParent();

    List<MenuItem> getMenuItemByParentSeqno(int seqno);
}
