package org.fpwei.bo.dao;

import org.apache.ibatis.annotations.Param;
import org.fpwei.bo.entity.Job;
import org.fpwei.bo.entity.QuartzCronTrigger;
import org.fpwei.bo.entity.QuartzTrigger;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by fpwei on 2017/1/2.
 */
@Repository
public interface QuartzDao {
    List<Job> getJobsByName(@Param("name") String jobName);

    List<Job> getAllJobs();

    List<QuartzCronTrigger> getAllCronTriggers();

    QuartzCronTrigger getCronTriggerByTriggerKey(@Param("name") String triggerName, @Param("group") String triggerGroup);

    QuartzTrigger getTriggerByTriggerKey(@Param("name") String triggerName, @Param("group") String triggerGroup);

    List<QuartzTrigger> getTriggerByJobName(@Param("name") String jobName);
}
