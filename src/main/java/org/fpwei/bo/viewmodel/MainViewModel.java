package org.fpwei.bo.viewmodel;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.NotifyChange;

/**
 * Created by fpwei on 2017/1/2.
 */
public class MainViewModel {
    private String currentPage = "WEB-INF/zul/info/news.zul";

    @GlobalCommand("selectNavigation")
    @NotifyChange("currentPage")
    public void changeCurrentPage(@BindingParam("path") String path) {
        if(path == null){
            return;
        }
        currentPage = path;
    }


    public String getCurrentPage() {
        return currentPage;
    }
}
