package org.fpwei.bo.viewmodel;

import org.fpwei.bo.dto.CronTriggerDto;
import org.fpwei.bo.dto.JobDto;
import org.fpwei.bo.service.ScheduleService;
import org.fpwei.common.webservice.Response;
import org.fpwei.common.webservice.SchedulerService;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Window;

import java.util.List;

/**
 * Created by fpwei on 2017/1/2.
 */
@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class ScheduleViewModel {
    @WireVariable
    private ScheduleService scheduleService;
    @WireVariable
    private SchedulerService schedulerService; //web service


    private List<JobDto> jobList;
    private List<CronTriggerDto> cronTriggerList;

    private String job;

    @Init
    public void init() {
        search();
    }

    @Command
    @NotifyChange({"jobList","cronTriggerList"})
    public void refresh() {
        search();
    }

    @Command("refreshJob")
    @NotifyChange("jobList")
    public void searchJob() {
        jobList = scheduleService.getJobsByName(job);
    }

    @Command("refreshTrigger")
    @NotifyChange("cronTriggerList")
    public void searchTrigger() {
        cronTriggerList = scheduleService.getCronTriggersByJobName(job);
    }


    @Command("search")
    @NotifyChange({"jobList","cronTriggerList"})
    public void search() {
        searchJob();
        searchTrigger();
    }

    @Command("openSchedulerContext")
    public void openSchedulerContext() {
        Window window = (Window)Executions.createComponents("/WEB-INF/zul/admin/schedulerContext.zul", null, null);
        window.setBorder(true);
        window.setClosable(true);
        window.setTitle("Scheduler Context");
        window.setHflex("min");
        window.setWidth("1000px");
        window.setSizable(true);
        window.doHighlighted();
    }


    //cxf - client
    @Command("start")
    public void startScheduler(){
        Response response = schedulerService.start();
        Clients.showNotification(response.getResult().getErrorCode() == 0 ? "start success":"start failed - " + response.getResult().getErrorMsg());
    }

    @Command("execute")
    public void executeJob(@BindingParam("jobName") String jobName){
        Response response = schedulerService.execute(jobName);
        Clients.showNotification(response.getResult().getErrorCode() == 0 ? "execute success":"execute failed - " + response.getResult().getErrorMsg());
    }

    @Command("updateTrigger")
    public void updateTrigger(@BindingParam("jobName") String jobName, @BindingParam("cronExpression") String cronExpression){
        Response response = schedulerService.update(jobName, cronExpression);
        Clients.showNotification(response.getResult().getErrorCode() == 0 ? "update success":"update failed - " + response.getResult().getErrorMsg());
    }


    //getter & setter
    public ListModelList<JobDto> getJobList() {
        return new ListModelList<JobDto>(jobList);
    }

    public ListModelList<CronTriggerDto> getCronTriggerList() {
        return new ListModelList<CronTriggerDto>(cronTriggerList);
    }

    public void setJob(String job) {
        this.job = job;
    }
}
