package org.fpwei.bo.viewmodel;

import org.fpwei.bo.dto.MenuItemDto;
import org.fpwei.bo.service.MenuItemService;
import org.zkoss.bind.annotation.Init;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.ListModelList;

import java.util.List;

/**
 * Created by fpwei on 2017/1/2.
 */
@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class SideBarViewModel {
    @WireVariable
    private MenuItemService menuItemService;

    private List<MenuItemDto> menuItemList;

    @Init
    public void init() {
        menuItemList = menuItemService.getNavigation();
    }

    public ListModelList<MenuItemDto> getMenuItemList() {
        return new ListModelList<MenuItemDto>(menuItemList);
    }
}
