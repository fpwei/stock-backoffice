package org.fpwei.bo.viewmodel;

import org.fpwei.common.webservice.Response;
import org.fpwei.common.webservice.SchedulerMetaDataDto;
import org.fpwei.common.webservice.SchedulerMetaDataResponse;
import org.fpwei.common.webservice.SchedulerService;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zk.ui.util.Clients;

/**
 * Created by fpwei on 2017/1/6.
 */
@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class SchedulerViewModel {
    @WireVariable
    private SchedulerService schedulerService;

    private SchedulerMetaDataDto metadata;

    @Init
    public void init() {
        getSchedulerMetaData();
    }

    @Command("start")
    @NotifyChange("metadata")
    public void startScheduler(){
        Response response = schedulerService.start();
        Clients.showNotification(response.getResult().getErrorCode() == 0 ? "start success":"start failed - " + response.getResult().getErrorMsg());
        getSchedulerMetaData();
    }

    @Command("pause")
    @NotifyChange("metadata")
    public void pauseScheduler(){
        Response response = schedulerService.pause();
        Clients.showNotification(response.getResult().getErrorCode() == 0 ? "pause success":"pause failed - " + response.getResult().getErrorMsg());
        getSchedulerMetaData();
    }

    @Command("shutdown")
    @NotifyChange("metadata")
    public void shutdownScheduler(){
        Response response = schedulerService.shutdown();
        Clients.showNotification(response.getResult().getErrorCode() == 0 ? "shutdown success":"shutdown failed - " + response.getResult().getErrorMsg());
        getSchedulerMetaData();
    }

    @Command("refresh")
    @NotifyChange("metadata")
    public void getSchedulerMetaData() {
        SchedulerMetaDataResponse response = schedulerService.getSchedulerMetaData();
        if(response.getResult().getErrorCode() == 0){
            metadata = response.getSchedulerMetaDataDto();
        }else{
            Clients.showNotification(response.getResult().getErrorMsg());
        }
    }

    public SchedulerMetaDataDto getMetadata() {
        return metadata;
    }
}
